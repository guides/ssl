creating ssl 

создание корневого сертификата
openssl req -new -x509 -extensions v3_ca -keyout /etc/ssl/private/cakey.pem -out /etc/ssl/certs/cacert.pem -days 7650

создать приватный ключ
openssl genrsa -out postgres.key 2048
openssl genrsa -out postgresclient.key 2048
создать файл запроса из приватного ключа
openssl req -new -key postgres.key -out postgres.csr
openssl req -new -key postgresclient.key -out postgresclient.csr
не нужно (просит пароль)
openssl genrsa -des3 -out server.key 2048


подписать
sudo openssl ca -in /home/rafael/ssl/postgres.csr -config /etc/ssl/openssl.cnf -days 5000
sudo openssl ca -in /home/rafael/ssl/postgresclient.csr -config /etc/ssl/openssl.cnf -days 5000
#postgrsclient
openssl genrsa -out postgresclient.key 2048
openssl req -new -key postgresclient.key -out postgresclient.csr
sudo openssl ca -in /home/rafael/ssl/postgresclient.csr -config /etc/ssl/openssl.cnf -days 5000

#smm_go
openssl genrsa -out smm_go.key 2048
openssl req -new -key smm_go.key -out smm_go.csr
sudo openssl ca -in /home/rafael/ssl/smm_go.csr -config /etc/ssl/openssl.cnf -days 5000

#smm_net
openssl genrsa -out smm_net.key 2048
openssl req -new -key smm_net.key -out smm_net.csr
sudo openssl ca -in /home/rafael/ssl/smm_net.csr -config /etc/ssl/openssl.cnf -days 5000

sudo -u postgres psql postgres
# \password postgres


openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt
openssl req -new -key server.key -out server.csr


cat server101.mycloud.key server101.mycloud.crt > server101.mycloud.pem
sudo mv cakey.pem /etc/ssl/private/
sudo mv cacert.pem /etc/ssl/certs/
корневой приватный
openssl genrsa -out rootCA.key 2048
корневой публичный
openssl req -x509 -new -key rootCA.key -days 8000 -out rootCA.crt  -config cert
сгенерировать приватный
openssl genrsa -out postgres.key 2048
запрос на подписку
openssl req -new -key postgres.key -out postgres.csr -config cert
подписать
openssl x509 -req -in postgres.csr -CA rootCA.crt -CAkey rootCA.key -CAcreateserial -out postgres.crt -days 5000


https://help.ubuntu.com/lts/serverguide/certificates-and-security.html



example config
[ req ]
default_bits                = 1024
default_keyfile             = privkey.pem
distinguished_name          = req_distinguished_name
req_extensions              = v3_req
 
[ req_distinguished_name ]
countryName                 = Country Name (2 letter code)
countryName_default         = UK
stateOrProvinceName         = State or Province Name (full name)
stateOrProvinceName_default = Wales
localityName                = Locality Name (eg, city)
localityName_default        = Cardiff
organizationName            = Organization Name (eg, company)
organizationName_default    = Example UK
commonName                  = Common Name (eg, YOUR name)
commonName_default          = one.test.app.example.net
commonName_max              = 64
 
[ v3_req ]
basicConstraints            = CA:FALSE
keyUsage                    = nonRepudiation, digitalSignature, keyEncipherment
subjectAltName              = @alt_names
 
[alt_names]
DNS.1   = two.test.app.example.net
DNS.2   = exampleapp.com